import random

from suitebot3.game.ant import Ant
from suitebot3.game.direction import Directions
from suitebot3.ai.bot_ai import BotAi
from suitebot3.game.game_setup import GameSetup
from suitebot3.game.game_state import GameState
from suitebot3.game.moves import Moves, MovesBuilder
from suitebot3.game.point import Point


class SampleBotAi(BotAi):

    DIRECTIONS = [ Directions.UP, Directions.DOWN, Directions.LEFT, Directions.RIGHT ]

    antType = {}

    startAntCount = 24
    startAntRound = 0

    incAntCount = 8
    incAntRound = 40

    killRoundStart = 60

    killSearchSize = 5

    checkSquareOffset = 1

    TYPES = ["resource", "kill"]

    recursionCount = 0

    goingToPositions = []

    def __init__(self, game_setup: GameSetup):
        ''' Called before each new game '''
        self.my_id = game_setup.ai_player_id
        self.antType = {}

    def createAnt(self, game_state: GameState, move: MovesBuilder):
        move.spawn_new_ant()
        index = len(game_state.get_ants_of_player(self.my_id))

        if game_state.get_current_round() >= self.killRoundStart:
            self.antType[ index ] = random.choice(self.TYPES)
        else:
            self.antType[ index ] = "resource"

    def calcSquareValue(self, position, direction, game_state):

        startPoint = self.getPositionInDirection(position, direction, game_state, self.checkSquareOffset + 1)

        resourceSum = 0

        for x in range(self.checkSquareOffset*-1, self.checkSquareOffset):
            for y in range(self.checkSquareOffset * -1, self.checkSquareOffset):
                field = game_state.get_field(Point(startPoint.x + x, startPoint.y + y))
                resourceSum += field.get_resource_count()

        return resourceSum

    def getFieldInDirection(self, position: Point, direction, game_state: GameState, distance = 1):
        if direction == Directions.UP:
            position = Point(position.x, position.y - distance)
        elif direction == Directions.DOWN:
            position = Point(position.x, position.y + distance)
        elif direction == Directions.RIGHT:
            position = Point(position.x + distance, position.y)
        elif direction == Directions.LEFT:
            position = Point(position.x - distance, position.y)

        return game_state.get_field(position)

    def getPositionInDirection(self, position: Point, direction, game_state: GameState, distance = 1):
        if direction == Directions.UP:
            position = Point(position.x, position.y - distance)
        elif direction == Directions.DOWN:
            position = Point(position.x, position.y + distance)
        elif direction == Directions.RIGHT:
            position = Point(position.x + distance, position.y)
        elif direction == Directions.LEFT:
            position = Point(position.x - distance, position.y)

        return position

    def calcDistance(self, myPos, toKillPos ):
        return abs(myPos.x - toKillPos.x) + abs(myPos.y - toKillPos.y)

    def getKillDirection(self, ant: Ant, game_state: GameState, antIndex):

        antPos = Ant.get_position()

        nearest = self.killSearchSize*self.killSearchSize + 99
        antToKill = None

        for x in range(self.killSearchSize*-1, self.killSearchSize):
            for y in range(self.killSearchSize * -1, self.killSearchSize):
                checkPoint = Point(antPos.x + x, antPos.y + y)
                field = game_state.get_field(checkPoint)
                checkAnt = field.get_ant()
                if checkAnt is not None:
                    if checkAnt.get_player() != self.my_id:
                        if nearest > self.calcDistance(antPos, checkPoint):
                            antToKill = checkAnt;
                            nearest = self.calcDistance(antPos, checkPoint)

        if antToKill is not None:
            killPos = antToKill.get_position()
            result = None
            if killPos.x != antPos.x:
                if killPos.x > antPos.x:
                    result = Directions.RIGHT
                else:
                    result = Directions.LEFT
            elif killPos.y != antPos.y:
                if killPos.y > antPos.y:
                    result = Directions.DOWN
                else:
                    result = Directions.UP

            field = self.getFieldInDirection(antPos, result)
            carryAnt = field.get_ant()
            if carryAnt is not None:
                if carryAnt.get_player() == self.my_id:
                    return None
            return result
        else:
            return None

    def getDirection(self, ant : Ant, game_state: GameState, antIndex, ignoreDirection = None):
        self.recursionCount += 1

        position = ant.get_position()

        up = self.getFieldInDirection(position, Directions.UP, game_state)
        down = self.getFieldInDirection(position, Directions.DOWN, game_state)
        left = self.getFieldInDirection(position, Directions.LEFT, game_state)
        right = self.getFieldInDirection(position, Directions.RIGHT, game_state)

        try:
            if self.antType[ antIndex ] == "kill":
                direction = self.getKillDirection(ant, game_state, antIndex)
                if direction is not None:
                    self.goingToPositions.append(self.getPositionInDirection(position, direction, game_state))
                    return direction
        except Exception:
            pass

        top = Directions.UP  # TODO
        topCount = self.calcSquareValue(position, Directions.UP, game_state)

        if ignoreDirection == Directions.UP:
            top = Directions.DOWN
            topCount = self.calcSquareValue(position, Directions.DOWN, game_state)


        if(topCount < self.calcSquareValue(position, Directions.DOWN, game_state) and ignoreDirection != Directions.DOWN):
            top = Directions.DOWN
            topCount = self.calcSquareValue(position, Directions.DOWN, game_state)

        if (topCount < self.calcSquareValue(position, Directions.LEFT, game_state) and ignoreDirection != Directions.LEFT):
            top = Directions.LEFT
            topCount = self.calcSquareValue(position, Directions.LEFT, game_state)

        if (topCount < self.calcSquareValue(position, Directions.RIGHT, game_state) and ignoreDirection != Directions.RIGHT):
            top = Directions.RIGHT
            topCount = self.calcSquareValue(position, Directions.RIGHT, game_state)

        topPosition = self.getPositionInDirection(position, top, game_state)

        if self.recursionCount > 3:
            return topPosition

        if self.isPointInArray(self.goingToPositions, topPosition):
            topPosition = self.getDirection(ant, game_state, antIndex, top)

        possiblyMyAnt = self.getFieldInDirection(position, topPosition, game_state).get_ant()
        if possiblyMyAnt is not None:
            topPosition = self.getDirection(ant, game_state, antIndex, top)

        possiblyBase = self.getFieldInDirection(position, topPosition, game_state).get_base()
        if possiblyBase is not None:
            topPosition = self.getDirection(ant, game_state, antIndex, top)

        self.goingToPositions.append(topPosition)

        return top

    def isPointInArray(self, array, needle):
        for item in array:
            if item.x == needle.x and item.y == needle.y:
                return True

        return False

    def oppositeDirection(self, direction):
        if direction == Directions.UP:
            return Directions.DOWN
        elif direction == Directions.DOWN:
            return Directions.UP
        elif direction == Directions.RIGHT:
            return Directions.LEFT
        else:
            return Directions.RIGHT

    def make_moves(self, game_state: GameState) -> Moves:

        self.goingToPositions = []

        moves = MovesBuilder(game_state, self.my_id)

        try:
            if len(game_state.get_ants_of_player(self.my_id)) <= self.startAntCount and game_state.get_current_round() >= self.startAntRound and game_state.get_current_round() < self.incAntRound:
                self.createAnt(game_state, moves)

            if len(game_state.get_ants_of_player(self.my_id)) <= self.incAntCount and game_state.get_current_round() >= self.incAntRound:
                self.createAnt(game_state, moves)
        except Exception:
            pass

        antIndex = 0
        for my_ant in game_state.get_ants_of_player(self.my_id):
            try:
                self.recursionCount = 0
                direction = self.getDirection(my_ant, game_state, antIndex, self.oppositeDirection(my_ant.get_last_move()))
            except Exception:
                direction = random.choice(self.DIRECTIONS)

            moves.move_ant(my_ant, direction)
            antIndex += 1

        return moves.build()
